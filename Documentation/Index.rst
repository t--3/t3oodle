﻿.. include:: Includes.txt


.. _start:

=============================================================
t3oodle
=============================================================

.. only:: html

	:Classification:
		t3oodle

	:Version:
		|release|

	:Language:
		en

	:Description:
		Simple poll extension for TYPO3 CMS. t3oodle allows your frontend users to create new polls and vote for existing ones.

	:Keywords:
		TYPO3 CMS, poll, doodle, vote

	:Copyright:
		2020

	:Author:
		Armin Vieweg

	:Email:
		info@v.ieweg.de

	:License:
		This document is published under the Open Content License available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3, a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.


	**Table of Contents**

.. toctree::
        :maxdepth: 2
        :titlesonly:

        Welcome/Index
        Support/Index
        Versions/Index


.. note::
   This extension has been supported by `Friedrich-Ebert-Stiftung e. V.<https://www.fes.de/>`_.
