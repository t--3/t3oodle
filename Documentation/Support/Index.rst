.. include:: ../Includes.txt


.. _support:


Support
=======

Issues
------

If you experienced issues with this extension or if you have a feature request, please visit the issue tracker on
Bitbucket: `<https://bitbucket.org/t--3/t3oodle/issues>`_


Donate
------

If you like the t3oodle extension, feel free to `donate`_ some funds to support further development.

.. _donate: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2DCCULSKFRZFU


Contribute
----------

If you are a developer and you want to submit improvements as code, you can fork
`<https://bitbucket.org/t--3/t3oodle>`_ and make a pull request to fluid_page_cache's master branch.

Thanks!
