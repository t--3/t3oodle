<?php declare(strict_types=1);
namespace T3\T3oodle\Domain\Permission;

/*  | The t3oodle extension is made with ❤ for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2020 Armin Vieweg <info@v.ieweg.de>
 */

class AccessDeniedException extends \T3\T3oodle\Exception\AccessDeniedException
{
}
